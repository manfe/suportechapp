import 'package:app_suporte/Models/Interesse.dart';
import 'package:app_suporte/Models/Post.dart';

class Item {
  Item({this.id, this.option, this.post, this.enabled, this.interesse});
  String id;
  String option;
  Post post;
  bool enabled;
  Interesse interesse;
}
