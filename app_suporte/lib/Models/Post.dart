import 'package:app_suporte/Models/Item.dart';
import 'package:app_suporte/Models/Interesse.dart';
import 'package:flutter/material.dart';

class Post {
  int postId;
  int servicoId;
  int userId;
  bool interesse;
  bool concluido;
  Color color;
  Icon icon;
  List<Interesse> interessePrestador;
  List<Item> listItem;
  String message;
  String serviceName;
  String descricao;
  String diaSemana;
  String horaInitial;
  String horaFinal;
  String solicitante;
  DateTime createdAt;
  DateTime updatedAt;

  Post(
      {this.descricao,
      this.postId,
      this.servicoId,
      this.createdAt,
      this.diaSemana,
      this.updatedAt,
      this.horaFinal,
      this.horaInitial,
      this.userId,
      this.concluido,
      this.color,
      this.listItem,
      this.icon,
      this.message,
      this.serviceName,
      this.solicitante,
      this.interessePrestador,
      this.interesse});

  factory Post.fromJson(Map<String, dynamic> json) {
    List<Interesse> list = new List<Interesse>();
    if (json['interesses'] != null) {
      for (var item in json['interesses']) {
        Interesse inter = new Interesse.fromJson(item);
        list.add(inter);
      }
    }

    return new Post(
      postId: json['post_id'],
      descricao: json["descricao"],
      horaInitial: json['hora_inicial'],
      horaFinal: json['hora_final'],
      userId: json['user_id'],
      servicoId: json['servico_id'],
      interesse: json['interesse'],
      interessePrestador: list,
      concluido: json['concluido'],
      serviceName: json['service_name'],
      diaSemana: json['dia_semana'],
      solicitante: json['solicitante'],
      createdAt: DateTime.parse(json['created_at']),
      updatedAt: DateTime.parse(json['updated_at']),
    );
  }

  Map<String, dynamic> toJson() => {
        'descricao': descricao,
        'horaInitial': horaInitial,
        'horaFinal': horaFinal,
        'userId': userId,
        'servicoId': servicoId,
        'diaSemana': diaSemana,
        'message': message
      };
}
