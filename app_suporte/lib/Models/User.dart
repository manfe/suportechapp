class User {
  int userId;
  String name;
  String endereco;
  String telefone;
  String celular;
  String email;
  String sexo;
  String cpf;
  String password;
  DateTime createdAt;
  DateTime updatedAt;

  User(
      {this.userId,
      this.name,
      this.endereco,
      this.telefone,
      this.celular,
      this.cpf,
      this.email,
      this.sexo,
      this.password,
      this.createdAt,
      this.updatedAt});

  factory User.fromJson(Map<String, dynamic> json) {
    return new User(
      userId: json['user_id'],
      name: json['name'],
      endereco: json['endereco'],
      telefone: json['telefone'],
      celular: json['celular'],
      email: json['email'],
      sexo: json['sexo'],
      cpf: json['cpf'],
      createdAt: DateTime.parse(json['created_at']),
      updatedAt: DateTime.parse(json['updated_at']),
    );
  }

  Map<String, dynamic> toJson() => {
        'name': name,
        'email': email,
        'endereco': endereco,
        'telefone': telefone,
        'celular': celular,
        'sexo': sexo,
        'cpf': cpf,
        'password': password,
        'createdAt': createdAt,
        'updatedAt': updatedAt
      };
}
