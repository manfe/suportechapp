import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;

import 'package:app_suporte/Code/global.dart';
import 'package:app_suporte/Models/Service.dart';

class ServiceController {
  Future<List<Service>> getServices() async {
    final response = await http.get(
      Global.url + '/api/service/' + Global.username,
      headers: {HttpHeaders.AUTHORIZATION: "Bearer " + Global.token},
    );
    final responseJson = json.decode(response.body);

    List<Service> services = new List();

    for (var item in responseJson["data"]) {
      Service service = Service.fromJson(item);
      services.add(service);
    }
    return services;
  }

  Future<bool> savePrestadorServices(
      List<int> serviceIds, int prestadorId) async {
    final response = await http.post(Global.url + '/api/servicePrestador',
        headers: {
          HttpHeaders.AUTHORIZATION: "Bearer " + Global.token,
          'Content-type': 'application/json',
          'Accept': 'application/json'
        },
        body: json
            .encode({"prestadorId": prestadorId, "serviceIds": serviceIds}));

    final responseJson = json.decode(response.body);
    return responseJson;
  }

  Future<bool> saveServicePrestado(int postId, int prestadorId) async {
    final response = await http.post(Global.url + '/api/servicePrestado',
        headers: {
          HttpHeaders.AUTHORIZATION: "Bearer " + Global.token,
          'Content-type': 'application/json',
          'Accept': 'application/json'
        },
        body: json.encode({"prestadorId": prestadorId, "postId": postId}));

    final responseJson = json.decode(response.body);
    return responseJson;
  }
}
