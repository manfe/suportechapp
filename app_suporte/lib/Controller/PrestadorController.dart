import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;

import 'package:app_suporte/Code/global.dart';
import 'package:app_suporte/Models/Prestador.dart';

class PrestadorController {
  Future<Prestador> savePrestador(String username) async {
    final response = await http.post(Global.url + '/api/prestador',
        headers: {HttpHeaders.AUTHORIZATION: "Bearer " + Global.token},
        body: {"username": username});

    final responseJson = json.decode(response.body);
    Prestador prestador = new Prestador.fromJson(responseJson["data"]);
    return prestador;
  }

  Future<bool> removeInteressePrestador(int interessePrestadorId) async {
    final response = await http.get(
        Global.url +
            '/api/interessePrestador/destroy/' +
            interessePrestadorId.toString(),
        headers: {
          HttpHeaders.AUTHORIZATION: "Bearer " + Global.token,
        });

    final responseJson = json.decode(response.body);

    if (responseJson) {
      return true;
    } else {
      return false;
    }
  }
}
