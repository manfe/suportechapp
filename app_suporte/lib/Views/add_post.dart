import 'package:app_suporte/Controller/ServiceController.dart';
import 'package:app_suporte/Controller/PostController.dart';
import 'package:app_suporte/Models/Service.dart';
import 'package:app_suporte/Models/Week.dart';
import 'package:app_suporte/ViewModel/PostViewModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';

class AddPost extends StatefulWidget {
  @override
  _State createState() => new _State();
}

class _State extends State<AddPost> {
  ServiceController serviceController = new ServiceController();
  PostController postController = new PostController();
  BuildContext _scaffoldContext;
  final formKey = new GlobalKey<FormState>();
  String _valueService = 'Serviço';
  PostViewModel _post = new PostViewModel(
      timeInitial: TimeOfDay.now(),
      timeFinal: TimeOfDay.now(),
      weekDays: <Week>[
        Week(name: 'Segunda'),
        Week(name: 'Terça'),
        Week(name: 'Quarta'),
        Week(name: 'Quinta'),
        Week(name: 'Sexta'),
        Week(name: 'Sábado'),
      ]);

  TextEditingController _timeInitialController = new TextEditingController();
  TextEditingController _timeFinalController = new TextEditingController();
  TextEditingController _optionController = new TextEditingController();

  String _addZeroIfNeeded(int value) {
    if (value < 10) return '0$value';
    return value.toString();
  }

  Future<Null> _selectTimeInitial(BuildContext context) async {
    final TimeOfDay picked =
        await showTimePicker(context: context, initialTime: _post.timeInitial);
    if (picked != null && picked != _post.timeInitial) {
      print('Tempo Selecionada: ${picked.toString()}');
      setState(() {
        _post.timeInitial = picked;
        _timeInitialController.text = _addZeroIfNeeded(picked.hour) +
            ":" +
            _addZeroIfNeeded(picked.minute);
      });
    }
  }

  Future<Null> _selectTimeFinal(BuildContext context) async {
    final TimeOfDay picked =
        await showTimePicker(context: context, initialTime: _post.timeFinal);
    print('Tempo Selecionada: ${picked.toString()}');
    if (picked != null && picked != _post.timeFinal) {
      setState(() {
        _post.timeFinal = picked;
        _timeFinalController.text = _addZeroIfNeeded(picked.hour) +
            ":" +
            _addZeroIfNeeded(picked.minute);
      });
    }
  }

  void _submit() {
    final form = formKey.currentState;
    if (form.validate()) {
      showDialog(
          barrierDismissible: false,
          context: context,
          builder: (_) => new Center(
                child: new CircularProgressIndicator(),
              ));

      form.save();
      postController.addPost(_post).then((bool result) {
        print("result is " + result.toString());
        if (result) {
          Navigator.pop(context);
          showDialog(
              barrierDismissible: false,
              context: context,
              builder: (_) => new SimpleDialog(
                    title: new Text(
                      "Post registrado com sucesso!",
                      textAlign: TextAlign.center,
                    ),
                    children: <Widget>[
                      new Container(
                        margin: const EdgeInsets.only(
                            left: 20.0, right: 20.0, top: 10.0),
                        child: RaisedButton(
                          onPressed: () {
                            Navigator.pop(context);
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Ok",
                            style: new TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18.0,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ));
        } else {
          Navigator.pop(context);
          showErrorMessage("putz algo ta errado");
        }
      }, onError: () {});
    } else {
      showErrorMessage("Todos os campos são obrigatórios!");
    }
  }

  void showErrorMessage(String message) {
    Scaffold.of(_scaffoldContext).showSnackBar(new SnackBar(
          content: new Text(message),
          duration: new Duration(seconds: 5),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text("Adicionar Post"),
        ),
        body: new Form(
          key: formKey,
          child: new ListView(
            children: <Widget>[
              new ListTile(
                leading: const Icon(Icons.description),
                title: new TextFormField(
                  keyboardType: TextInputType.multiline,
                  maxLines: 3,
                  decoration: new InputDecoration(
                    hintText: "Descrição",
                  ),
                  validator: (String value) {
                    if (value.isEmpty) return 'Descrição é obrigatória';
                    return null;
                  },
                  onSaved: (val) => _post.descricao = val,
                ),
              ),
              new ListTile(
                leading: const Icon(Icons.build),
                title: new FutureBuilder<List<Service>>(
                  future: serviceController.getServices(),
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    if (snapshot.hasData) {
                      List<Service> services = snapshot.data;
                      return new DropdownButton<Service>(
                        hint: Text(_valueService),
                        onChanged: (Service service) {
                          _post.service = service;
                          setState(() {
                            _optionController.text = service.name;
                            _valueService = service.name;
                          });
                        },
                        items: services.map((Service service) {
                          return new DropdownMenuItem<Service>(
                            value: service,
                            child: new Text(service.name),
                          );
                        }).toList(),
                      );
                    } else if (snapshot.hasError) {
                      return new Text("${snapshot.error}");
                    }
                    return new DropdownButton(
                      items: <DropdownMenuItem>[
                        new DropdownMenuItem(
                          child: new Text(''),
                        )
                      ],
                      onChanged: (value) {},
                    );
                  },
                ),
              ),
              new ListTile(
                leading: const Icon(Icons.access_time),
                title: new TextFormField(
                  enabled: false,
                  controller: _timeInitialController,
                  decoration: new InputDecoration(
                    hintText: "Hora Inicial",
                  ),
                  validator: (String value) {
                    if (value.isEmpty) return 'Valor Obrigatório';
                    return null;
                  },
                ),
                trailing: new Container(
                  decoration: new BoxDecoration(
                    borderRadius: BorderRadius.all(const Radius.circular(5.0)),
                    color: Colors.brown[300],
                  ),
                  height: 45.0,
                  width: 45.0,
                  child: new InkWell(
                    child: Icon(Icons.access_time, color: Colors.black),
                    onTap: () {
                      _selectTimeInitial(context);
                    },
                  ),
                ),
              ),
              new ListTile(
                leading: const Icon(Icons.access_time),
                title: new TextFormField(
                  enabled: false,
                  controller: _timeFinalController,
                  decoration: new InputDecoration(
                    hintText: "Hora Final",
                  ),
                  validator: (String value) {
                    if (value.isEmpty) return 'Valor Obrigatório';
                    return null;
                  },
                ),
                trailing: new Container(
                  decoration: new BoxDecoration(
                    borderRadius: BorderRadius.all(const Radius.circular(5.0)),
                    color: Colors.brown[300],
                  ),
                  height: 45.0,
                  width: 45.0,
                  child: new InkWell(
                      child: Icon(
                        Icons.access_time,
                        color: Colors.black,
                      ),
                      onTap: () {
                        _selectTimeFinal(context);
                      }),
                ),
              ),
              new ListTile(
                leading: const Icon(Icons.date_range),
                title: new Row(
                  children: <Widget>[
                    new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Row(
                          children: <Widget>[
                            Checkbox(
                              value: _post.weekDays[0].checked,
                              onChanged: (bool value) {
                                setState(() {
                                  _post.weekDays[0].checked = value;
                                });
                              },
                            ),
                            Text(_post.weekDays[0].name),
                          ],
                        ),
                        new Row(children: <Widget>[
                          Checkbox(
                            value: _post.weekDays[2].checked,
                            onChanged: (bool value) {
                              setState(() {
                                _post.weekDays[2].checked = value;
                              });
                            },
                          ),
                          Text(_post.weekDays[2].name),
                        ]),
                        new Row(
                          children: <Widget>[
                            Checkbox(
                              value: _post.weekDays[4].checked,
                              onChanged: (bool value) {
                                setState(() {
                                  _post.weekDays[4].checked = value;
                                });
                              },
                            ),
                            Text(_post.weekDays[4].name),
                          ],
                        ),
                      ],
                    ),
                    new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Row(children: <Widget>[
                          Checkbox(
                            value: _post.weekDays[1].checked,
                            onChanged: (bool value) {
                              setState(() {
                                _post.weekDays[1].checked = value;
                              });
                            },
                          ),
                          Text(_post.weekDays[1].name),
                        ]),
                        new Row(
                          children: <Widget>[
                            Checkbox(
                              value: _post.weekDays[3].checked,
                              onChanged: (bool value) {
                                setState(() {
                                  _post.weekDays[3].checked = value;
                                });
                              },
                            ),
                            Text(_post.weekDays[3].name),
                          ],
                        ),
                        new Row(
                          children: <Widget>[
                            Checkbox(
                              value: _post.weekDays[5].checked,
                              onChanged: (bool value) {
                                setState(() {
                                  _post.weekDays[5].checked = value;
                                });
                              },
                            ),
                            Text(_post.weekDays[5].name),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              new Container(
                margin: const EdgeInsets.only(top: 40.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new RaisedButton(
                      child: new Text(
                        'Salvar',
                        style: new TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18.0,
                          color: Colors.white,
                        ),
                      ),
                      onPressed: () {
                        _submit();
                      },
                    ),
                  ],
                ),
              ),
              new Builder(builder: (BuildContext context) {
                _scaffoldContext = context;
                return new Center();
              })
            ],
          ),
        ));
  }
}
